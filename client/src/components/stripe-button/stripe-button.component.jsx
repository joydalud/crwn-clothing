import React from 'react';
import StripeCheckout from 'react-stripe-checkout';
import axios from 'axios';

const StripeCheckoutButton = ({price}) => {
	const priceForStripe = price * 100;
	const publishableKey = 'pk_test_51JmC9iJXosnt5xEd1sICGrMzd7ULKrXZy8QaxE1PspjkAc8DX1EkEJDXfpluA8PplUrU9wBRGEL1PxcOFregRjdZ00eEiFI1zx';
	
	const onToken = (token) => {
		axios({
			url: 'payment',
			method: 'post',
			data: {
				amount: priceForStripe,
				token
			}
		}).then(response => {
			alert('Payment Successful');
		}).catch(error => {
			console.log('Payment error: ', JSON.parse(error));
			alert('There was an issue with your payment. Please use the provided credit card.');
		});
	}
	
	return (
		<StripeCheckout 
			label="Pay Now"
			name="CRWN Clothing Ltd."
			billingAddress
			shippingAddress
			image="https://tinyurl.com/wrx7pv72"
			description={`Your total is $${price}`}
			amount={priceForStripe}
			panelLabel="Pay Now"
			token={onToken}
			stripeKey={publishableKey}
		/>
	)
}

export default StripeCheckoutButton;
