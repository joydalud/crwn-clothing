import React from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import { toggleCartHidden } from '../../redux/cart/cart.actions';
import { selectCartItemsCount } from '../../redux/cart/cart.selectors';
// import {ReactComponent as ShoppingIcon} from '../../assets/shopping-bag.svg';
// import './cart-icon.styles.scss';

// const CartIcon = ({toggleCartHidden, itemCount}) => (
//     <div className='cart-icon' onClick={toggleCartHidden}>
//         <ShoppingIcon className='shopping-icon'/>
//         <span className='item-count'>{itemCount}</span>
//     </div>
// );
import {
    CartContainer,
    ShoppingIcon,
    ItemCountContainer
  } from './cart-icon.styles';
  
  const CartIcon = ({ toggleCartHidden, itemCount }) => (
    <CartContainer onClick={toggleCartHidden}>
      <ShoppingIcon />
      <ItemCountContainer>{itemCount}</ItemCountContainer>
    </CartContainer>
  );
const mapStatToProps = createStructuredSelector ({
    itemCount: selectCartItemsCount
});

const mapDispatchToProps = dispatch => ({
    toggleCartHidden: () => dispatch(toggleCartHidden())
});

export default connect(mapStatToProps, mapDispatchToProps)(CartIcon);